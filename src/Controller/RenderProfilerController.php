<?php

namespace Drupal\render_profiler\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\render_profiler\RenderProfilerLogger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Render profiler routes.
 */
class RenderProfilerController extends ControllerBase {

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default')
    );
  }

  public function title(string $id) {
    return Html::escape('Render Profiler run: ' . $id);
  }

  /**
   * Builds the response.
   */
  public function build(string $id) {
    $cache = $this->cache->get('render_profiler:' . $id);
    if (!isset($cache->data)) {
      throw new NotFoundHttpException();
    }
    RenderProfilerLogger::setEntries($cache->data);
    $results = RenderProfilerLogger::buildResultsTable();
    return [
      '#theme' => 'render_profiler_results',
      '#results' => $results,
    ];
  }

}
