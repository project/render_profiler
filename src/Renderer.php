<?php

namespace Drupal\render_profiler;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\PlaceholderGeneratorInterface;
use Drupal\Core\Render\RenderCacheInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Override's Drupal core's Renderer service to implement tracing throughout the
 * render tree.
 */
class Renderer extends \Drupal\Core\Render\Renderer {

  /**
   * @var \Drupal\render_profiler\RunSettings
   */
  protected RunSettings $runSettings;

  /**
   * @var \Drupal\render_profiler\Tracer
   */
  protected Tracer $tracer;

  /**
   * Override constructor to add runSettings and Tracer services.
   */
  public function __construct(ControllerResolverInterface $controller_resolver, ThemeManagerInterface $theme, ElementInfoManagerInterface $element_info, PlaceholderGeneratorInterface $placeholder_generator, RenderCacheInterface $render_cache, RequestStack $request_stack, array $renderer_config) {
    parent::__construct($controller_resolver, $theme, $element_info, $placeholder_generator, $render_cache, $request_stack, $renderer_config);
    $this->runSettings = \Drupal::service('render_profiler.run_settings');
    $this->tracer = \Drupal::service('render_profiler.tracer');
  }

  /**
   * @param $elements
   * @param $is_root_call
   *
   * @return \Drupal\Component\Render\MarkupInterface|mixed|string|void
   */
  protected function doRender(&$elements, $is_root_call = FALSE) {
    // Just do regular rendering if we're not in a profiling run.
    if (!$this->runSettings->isActive()) {
      return parent::doRender($elements, $is_root_call);
    }
    $this->pauseTimers();
    $this->tracer->incrementCount();
    // Since the count will change as we descend into the rendering tree, we
    // need to locally store which count we were on originally.
    $current_count = $this->tracer->getCount();
    $current_depth = $this->tracer->getDepth();
    // Log the initial cache metadata.
    if (!empty($elements['#cache'])) {
      RenderProfilerLogger::log($current_count, [
        'cache_start' => $elements['#cache'],
      ]);
      // Let items in the trace know this is where cache metadata was set.
      foreach (['tags', 'contexts'] as $metadata_type) {
        if (!empty($elements['#cache'][$metadata_type])) {
          foreach ($elements['#cache'][$metadata_type] as $lineitem) {
            foreach ($this->tracer->getTrace() as $parent_count) {
              RenderProfilerLogger::log($parent_count, [
                'cache_trace' => [
                  $metadata_type => [
                    $lineitem => [$current_count],
                  ],
                ],
              ]);
            }
          }
        }
      }
    }

    // Determine the type/subtype of renderable, and whether to show a visible
    // annotation of the profiling in the markup.
    $type = $elements['#type'] ?? $elements['#theme'] ?? NULL;
    $subtype = NULL;
    $annotate = TRUE;
    if (is_array($type)) {
      $subtype = reset($type);
      $type = end($type);
    }
    switch ($type) {
      case 'block':
        $subtype = $elements['#plugin_id'];
        break;
      case 'field':
        $subtype = $elements['#field_name'];
        break;
      case 'form':
        $subtype = $elements['#form_id'];
        break;
      case 'html_tag':
        $subtype = $elements['#tag'];
        break;
      case 'node':
        $subtype = $elements['#node']->bundle();
        break;
      case 'paragraph':
        $subtype = $elements['#paragraph']->bundle();
        break;
      case 'view':
        $subtype = (isset($elements['#view']))
          ? $elements['#view']->id()
          : $elements['#view_id'] ?? $elements['#name'];
        $subtype .= ':' . $elements['#display_id'];
        break;
      case 'markup':
        $annotate = FALSE;
        break;
      case NULL:
        $annotate = FALSE;
        if (!empty($elements['#markup'])) {
          $type = 'markup';
        }
        elseif (Element::children($elements)) {
          $type = '(array)';
        }
        elseif (!empty($elements['#pre_render'])) {
          $type = 'pre_render';
        }
        elseif (!empty($elements['#lazy_builder'])) {
          $type = 'lazy_builder';
          $subtype = reset($elements['#lazy_builder']);
          if ($subtype == 'Drupal\block\BlockViewBuilder::lazyBuilder') {
            $subtype = 'BlockViewBuilder: ' . implode(':', $elements['#lazy_builder'][1]);
          }
        }
        elseif (!empty($elements['#attached'])) {
          $type = '(attached)';
        }
        elseif (!empty($elements['#cache'])) {
          $type = '(cache metadata)';
        }
        elseif (empty($elements)) {
          $type = '(empty)';
        }
        else {
          $type = '???';
        }
      default:
    }

    // Temporarily end query logging for the parent iteration to prevent double
    // logging.
    if ($current_depth > 0) {
      $parent_count = $this->tracer->getTrace()[$current_depth - 1];
      $parent_queries = Database::getLog('render_profiler:' . $parent_count);
      RenderProfilerLogger::log($parent_count, [
        'queries' => $parent_queries,
      ]);
    }

    // Recurse into the $elements renderable and begin query logging for the
    // current iteration.
    Database::startLog('render_profiler:' . $current_count);
    $this->restartTimers();
    $this->tracer->descend();
    Timer::start('render_profiler:' . $current_count);
    $return_value = parent::doRender($elements, $is_root_call);
    $time = Timer::stop('render_profiler:' . $current_count)['time'];
    $this->tracer->ascend();
    $this->pauseTimers();
    // Stop query logging for this iteration and log the queries performed.
    $current_queries = Database::getLog('render_profiler:' . $current_count);
    RenderProfilerLogger::log($current_count, [
      'queries' => $current_queries,
    ]);
    // Restart query logging for the parent iteration.
    if (isset($parent_count)) {
      Database::startLog('render_profiler:' . $parent_count);
    }

    // Check if annotations are disabled for the run.
    if (!$this->runSettings->isShowingAnnotations()) {
      $annotate = FALSE;
    }
    // Record this rendering to the log.
    RenderProfilerLogger::log($current_count, [
      'count' => $current_count,
      'last_child' => $this->tracer->getCount(),
      'trace' => $this->tracer->getTrace(),
      'depth' => $current_depth,
      'type' => $type,
      'subtype' => $subtype,
      'time' => $time,
      'annotated' => $annotate,
    ]);
    // Also record the final cache metadata, after bubbling.
    if (!empty($elements['#cache'])) {
      RenderProfilerLogger::log($current_count, [
        'cache_end' => $elements['#cache'],
      ]);
    }

    // Prepend a visible annotation to the element on the page.

    if ($annotate) {
      $hidden = $time > $this->runSettings->getAnnotationThreshold() ? '' : 'hidden';
      $return_value = (is_object($return_value)) ? $return_value->__toString() : $return_value;
      $return_value = Markup::create("<div class='rp-annotation' data-rp-count='{$current_count}' {$hidden} style='z-index: calc(10000 - {$current_depth})'>{$time} {$type}</div>" . $return_value);
      if (!empty($elements['#markup'])) {
        $elements['#markup'] = $return_value;
      }
    }

    $this->restartTimers();
    return $return_value;
  }

  /**
   * Temporarily stop all running timers in the current trace.
   *
   * @return void
   */
  protected function pauseTimers() {
    foreach($this->tracer->getTrace() as $count) {
      Timer::stop('render_profiler:' . $count);
    }
  }

  /**
   * Restart timers in the current trace.
   *
   * @return void
   */
  protected function restartTimers() {
    foreach (array_reverse($this->tracer->getTrace()) as $count) {
      Timer::start('render_profiler:' . $count);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function replacePlaceholders(array &$elements) {
    if (!isset($elements['#attached']['placeholders']) || empty($elements['#attached']['placeholders'])) {
      return FALSE;
    }

    // Status messages and the render profiler log must both be processed after all other placeholders.
    $deferred_placeholders = [];
    foreach ($elements['#attached']['placeholders'] as $placeholder => $placeholder_element) {
      if (isset($placeholder_element['#lazy_builder']) && $placeholder_element['#lazy_builder'][0] === 'Drupal\Core\Render\Element\StatusMessages::renderMessages') {
        $deferred_placeholders[] = $placeholder;
      }
      elseif (isset($placeholder_element['#lazy_builder']) && $placeholder_element['#lazy_builder'][0] === 'Drupal\render_profiler\RenderProfilerLogger::build_log') {
        $deferred_placeholders[] = $placeholder;
      }
      else {
        $elements = $this->renderPlaceholder($placeholder, $elements);
      }
    }

    // Then render deferred placeholders.
    foreach ($deferred_placeholders as $placeholder) {
      $elements = $this->renderPlaceholder($placeholder, $elements);
    }

    return TRUE;
  }

}
