<?php

namespace Drupal\render_profiler;


use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class RunSettings {

  protected Request $request;

  protected AccountProxyInterface $currentUser;

  protected ImmutableConfig $config;

  protected CacheBackendInterface $cache;

  protected bool|null $active = NULL;

  public function __construct(RequestStack $request_stack, AccountProxyInterface  $current_user, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache
  ) {
    $this->request = $request_stack->getCurrentRequest();
    $this->currentUser = $current_user;
    $this->config = $config_factory->get('render_profiler.settings');
    $this->cache = $cache;
  }

  /**
   * Check if the profiler is active for this run.
   *
   * @return bool
   */
  public function isActive(): bool {
    if (is_null($this->active)) {
      $this->active = $this->hasQueryParameter() && $this->hasPermission();
    }
    return $this->active;
  }


  /**
   * Whether or not to render the profile results in a table at the bottom of
   * the page.
   *
   * @return bool
   */
  public function isShowingTable(): bool {
    $params = $this->request->query;
    return !$params->has('RENDER_PROFILER_SILENT');
  }

  /**
   * Whether or not to show id & timing annotations on top of elements.
   *
   * @return bool
   */
  public function isShowingAnnotations(): bool {
    $params = $this->request->query;
    if ($params->has('RENDER_PROFILER_SILENT')) {
      return FALSE;
    }
    if ($params->has('RP_ANNOTATIONS')) {
      return $params->get('RP_ANNOTATIONS');
    }
    return $this->config->get('display_annotations');
  }

  /**
   * The minimum render time for an element to show an annotation, in
   * milliseconds.
   *
   * @return int
   */
  public function getAnnotationThreshold(): int {
    $params = $this->request->query;
    if ($params->has('RP_THRESHOLD')) {
      return $params->get('RP_THRESHOLD');
    }
    return $this->config->get('threshold');
  }

  /**
   * Get the id specified in the query parameter, if present.
   *
   * @return string|null
   */
  public function id(): string|null {
    $params = $this->request->query;
    return $params->get('RENDER_PROFILER')
      ?? $params->get('RENDER_PROFILER_SILENT');
  }

  /**
   * Stop logging and save the log results to cache.
   *
   * @return void
   */
  public function stop() {
    // Cache the result and stop the run.
    if ($id = $this->id()) {
      $this->cache->set("render_profiler:$id", RenderProfilerLogger::getEntries(), CacheBackendInterface::CACHE_PERMANENT, ['rendered']);
    }
    $this->active = FALSE;
  }

  /**
   * Check if an enabling query parameter is present.
   *
   * @return bool
   */
  protected function hasQueryParameter(): bool {
    // Check if the query parameter is present.
    $params = $this->request->query;
    return
      $params->has('RENDER_PROFILER')
      || $params->has('RENDER_PROFILER_START')
      || $params->has('RENDER_PROFILER_SILENT');
  }

  /**
   * Check if the current user is permitted to use the profiler, or if there
   * a configuration setting to bypass permissions.
   *
   * @return bool
   */
  protected function hasPermission(): bool {
    if ($this->config->get('bypass_permission_check')) {
      return TRUE;
    }
    return $this->currentUser->hasPermission('use render_profiler');
  }

}
