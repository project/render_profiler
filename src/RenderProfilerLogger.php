<?php

namespace Drupal\render_profiler;

use Drupal\Core\Render\Markup;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * RenderProfilerLogger service.
 */
class RenderProfilerLogger implements TrustedCallbackInterface {

  protected static $log = [];

  public static function trustedCallbacks() {
    return [
      'build_log',
    ];
  }

  /**
   * Method description.
   */
  public static function log(int $count, array $entry) {
    if (isset(static::$log[$count])) {
      static::$log[$count] = array_merge_recursive(static::$log[$count], $entry);
    }
    else {
      static::$log[$count] = $entry;
    }
  }

  public static function setEntries(array $entries) {
    static::$log = $entries;
  }

  public static function getEntries($threshold = 0) {
    ksort(static::$log);
    return array_filter(static::$log, function($entry) use ($threshold) {
      return !empty($entry['time']) && $entry['time'] > $threshold;
    });
  }

  public static function getEntry($id) {
    return static::$log[$id];
  }

  public static function buildResultsTable() {
    $config = \Drupal::config('render_profiler.settings');
    $table = [
      '#type' => 'table',
      '#header' => [
        t('Order'),
        t('Depth'),
        t('Type'),
        t('Subtype'),
        t('Total time (ms)'),
        t('Query time (#)'),
        t('Tags'),
        t('Contexts'),
//        t('max-age'),
//        t('Keys'),
      ],
      '#attributes' => [
        'id' => 'rp-table',
        'class' => [
          'rp-table',
        ],
      ],
      '#attached' => [
        'library' => [
          'render_profiler/render_profiler'
        ],
      ],
    ];
    $entries = static::getEntries();
    foreach ($entries as $count => $item) {
      if (!$config->get('display_cache_metadata') && $item['type'] == '(cache metadata)') {
        continue;
      }
      $table[$count]['#attributes'] = [
        'id' => 'rp-row-' . $count,
        'data-rp-count' => $count,
        'data-rp-last-child' => $item['last_child'],
        'data-rp-annotated' => $item['annotated'] ? 'true' : 'false',
      ];
      $table[$count]['count'] = [
        '#plain_text' => $count,
        '#wrapper_attributes' => [
          'class' => ['rp-count'],
        ],
      ];
      $table[$count]['depth'] = [
        '#plain_text' => str_repeat('-', $item['depth']) . ' ' . $item['depth'],
        '#wrapper_attributes' => [
          'class' => ['rp-depth'],
        ],
      ];
      $table[$count]['type'] = [
        '#plain_text' => str_repeat('-', $item['depth']) . ' ' . $item['type'],
        '#wrapper_attributes' => [
          'class' => ['rp-type'],
        ],
      ];
      if ($item['subtype']) {
        $table[$count]['subtype'] = static::build_truncated_string($item['subtype']) + [
          '#wrapper_attributes' => [
            'class' => ['rp-subtype'],
          ],
        ];
      }
      else {
        $table[$count]['subtype'] = [];
      }
      $table[$count]['total_time'] = [
        '#plain_text' => str_repeat('-', $item['depth']) . ' ' . round($item['time'], 2),
        '#wrapper_attributes' => [
          'class' => ['rp-total-time'],
        ],
      ];
      $table[$count]['query_time'] = [
        '#plain_text' => static::get_query_time($item['queries']),
        '#wrapper_attributes' => [
          'class' => ['rp-query-time'],
        ],
      ];
      $table[$count]['tags'] = static::build_cache_details($item, 'tags') + [
        '#wrapper_attributes' => [
          'class' => ['rp-tags'],
        ],
      ];
      $table[$count]['contexts'] = static::build_cache_details($item, 'contexts') + [
        '#wrapper_attributes' => [
          'class' => ['rp-contexts'],
        ],
      ];
//      $table[$count]['max_age'] = [
//        '#plain_text' => $item['cache_end']['max-age'] ?? '',
//        '#wrapper_attributes' => [
//          'class' => ['rp-max-age'],
//        ],
//      ];
//      if (!empty($item['cache_end']['keys'])) {
//        $keys = implode(':', $item['cache_end']['keys']);
//        $table[$count]['keys'] = static::build_truncated_string($keys) + [
//          '#wrapper_attributes' => [
//            'class' => ['rp-keys'],
//          ],
//        ];
//      }
    }
    return $table;
  }

  public static function build_truncated_string($string) {
    if (strlen($string) < 20) {
      return [
        '#plain_text' => $string,
      ];
    }
    return [
      '#type' => 'details',
      '#title' => substr($string, 0, 20) . '&hellip;',
      '#attributes' => [
        'class' => ['rp-plain-details'],
      ],
      'content' => [
        '#plain_text' => $string,
      ],
    ];
  }

  public static function build_cache_details(array $current_entry, $metadata_key) {
    $specified_items = $current_entry['cache_start'][$metadata_key] ?? [];
    $trace_data = $current_entry['cache_trace'][$metadata_key] ?? [];
    if (empty($trace_data) && empty($specified_items)) {
      return [];
    }
    sort($specified_items);
    ksort($trace_data);

    $inherited_items = [];
    foreach ($trace_data as $cache_lineitem => $originating_ids) {
      $links = [];
      foreach ($originating_ids as $originating_id) {
        $entry = static::getEntry($originating_id);
        $link_text = $originating_id . ' ' . $entry['type'];
        $link_text .= !empty($entry['subtype']) ? ':' . $entry['subtype'] : '';
        $link_url = '#rp-row-' . $originating_id;
        $links[] = Markup::create("<a href='$link_url'>$link_text</a>");
      }
      $inherited_items[] = [
        '#type' => 'details',
        '#title' => $cache_lineitem,
        '#attributes' => [
          'class' => ['rp-cache-lineitem-details'],
        ],
        'content' => [
          '#theme' => 'item_list',
          '#items' => $links,
        ],
      ];
    }
    return [
      '#theme' => 'render_profiler_cache_details',
      '#title' => $metadata_key,
      '#specified_items' => $specified_items,
      '#inherited_items' => $inherited_items,
    ];
  }

  public static function build_log() {
    $runSettings = \Drupal::service('render_profiler.run_settings');
    $runSettings->stop();
    if ($runSettings->isShowingTable()) {
      $results = static::buildResultsTable();
      return [
        '#theme' => 'render_profiler_results',
        '#results' => $results,
      ];
    }
    return [];
  }

  /**
   * @param $queries
   *
   * @return string
   */
  protected static function get_query_time($queries): string {
    $num_queries = count($queries);
    $query_time = 0;
    foreach ($queries as $query) {
      $query_time += $query['time'];
    }
    $query_time = round($query_time * 1000, 2);
    $query_output = (!empty($num_queries)) ? $query_time . " ({$num_queries})" : '-';
    return $query_output;
  }

}
