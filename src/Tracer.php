<?php

namespace Drupal\render_profiler;

class Tracer {

  protected int $count = 0;

  protected int $depth = 0;

  protected array $trace = [];

  /**
   * @return int
   */
  public function getCount(): int {
    return $this->count;
  }

  /**
   * @return int
   */
  public function getDepth(): int {
    return $this->depth;
  }

  /**
   * @return array
   */
  public function getTrace(): array {
    return $this->trace;
  }

  /**
   * @return int
   */
  public function incrementCount(): int {
    $this->count++;
    return $this->count;
  }

  /**
   * @return int
   */
  public function descend(): int {
    $this->depth++;
    $this->trace[] = $this->getCount();
    return $this->depth;
  }

  /**
   * @return int
   */
  public function ascend(): int {
    $this->depth--;
    array_pop($this->trace);
    return $this->depth;
  }

}
