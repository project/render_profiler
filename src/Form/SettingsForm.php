<?php

namespace Drupal\render_profiler\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Render profiler settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'render_profiler_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['render_profiler.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['display_cache_metadata'] = [
      '#type' => 'checkbox',
      '#title' => t('Display cache metadata entries'),
      '#description' => t('If enabled, entries that only exist to show cache metadata will be shown in the profiler results. This metadata can be viewed on the parent entry directly, so this is usually unnecessary.'),
      '#default_value' => $this->config('render_profiler.settings')
        ->get('display_cache_metadata'),
    ];
    $form['display_annotations'] = [
      '#type' => 'checkbox',
      '#title' => t('Display annotations'),
      '#description' => t('If enabled, rendered elements will be annotated on the page with their render time.'),
      '#default_value' => $this->config('render_profiler.settings')->get('display_annotations'),
    ];
    $form['threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Reporting threshold (in milliseconds)'),
      '#description' => t('Limits the shown annotations to elements with a render time longer than the given value. "Display annotations" must be enabled.'),
      '#min' => 0,
      '#default_value' => $this->config('render_profiler.settings')->get('threshold'),
    ];
    $form['bypass_permission_check'] = [
      '#type' => 'checkbox',
      '#title' => t('Bypass permission check'),
      '#description' => t('If enabled, anyone will be able to use and see the profiler. Only enable this in dev environments.'),
      '#default_value' => $this->config('render_profiler.settings')
        ->get('bypass_permission_check'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('render_profiler.settings')
      ->set('display_cache_metadata', $form_state->getValue('display_cache_metadata'))
      ->set('display_annotations', $form_state->getValue('display_annotations'))
      ->set('threshold', $form_state->getValue('threshold'))
      ->set('bypass_permission_check', $form_state->getValue('bypass_permission_check'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
