CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage
 * Important Notes from Maintainer
 * Maintainers


INTRODUCTION
------------

Render Profiler is a development tool to help visualize and debug the render
tree and cacheability metadata for a given page,

 * Display a page's render tree in a collapsible UI, showing render and query
   times for each item
 * Collects and traces bubbled cache metadata, allowing you to see where
   any given tag or context originated from
 * Optionally annotates slow rendering elements on the page, and discovers
   elements that were renderer but not printed.


REQUIREMENTS
------------

This module has no dependencies outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


USAGE
-----

 * Ensure the "Use Render Profiler" permission is set for the appropriate roles
   on Drupal's permissions page
 * Optionally configure annotation settings in "Admin > Configuration >
   Development > Render Profiler settings"
 * Add the query parameter "RENDER_PROFILER_START=1" to any page URL to run
   the profiler on that page; results will be shown at page bottom
 * Alternatively use the query parameter "RENDER_PROFILER_START_SILENT=1" to
   run the profiler without altering the page output; view the saved results at
   /render-profiler/1 (you may use any string for the parameter value)


IMPORTANT NOTES FROM MAINTAINER
-------------------------------

 * Annotations get cached in render, page, and dynamic_page_cache bins. It may
   be a good idea to disable these bins in settings.local.php if you have them
   enabled.

 * Be aware that for any rendered thing, there is render time and there is
   build time. This module faithfully presents a tree branch's render time, but
   there was probably build time for that branch that would be represented
   somewhere shallower on the tree.


BUG REPORTS
-----------

To submit bug reports and feature suggestions, or track changes:
https://www.drupal.org/project/issues/render_profiler


MAINTAINERS
-----------

Current maintainers:
 * Les Lim - https://www.drupal.org/u/les-lim
