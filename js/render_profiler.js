(function (Drupal, drupalSettings) {
  Drupal.behaviors.render_profiler_highlight_not_printed = {
    attach: function (context) {
      context.querySelectorAll('tr[data-rp-annotated="true"]').forEach(row => {
        let count = row.getAttribute('data-rp-count');
        if (document.querySelectorAll(`div[data-rp-count="${count}"]`).length == 0) {
          row.classList.add('rp--is-not-printed');
        }
        else {
          row.classList.remove('rp--is-not-printed');
        }
      });
    }
  };

  Drupal.behaviors.render_profiler_add_collapse_controls = {
    attach: function (context) {
      document.querySelectorAll('.rp-table:not(.rp-controls-processed) tr[data-rp-last-child]').forEach(row => {
        let count = row.getAttribute('data-rp-count');
        let last_child = row.getAttribute('data-rp-last-child');
        let button = document.createElement('button');
        button.classList.add('rp-toggle');
        button.setAttribute('aria-expanded', 'true');
        button.setAttribute('onclick', `toggleRenderProfilerRows(${count}, ${last_child});`);
        button.textContent = '-';
        row.querySelector('td.rp-count').prepend(button);
      });
      document.querySelector('.rp-table').classList.add('rp-controls-processed');
    }
  };

})(Drupal, drupalSettings);

function toggleRenderProfilerRows(parent_count, last_child) {
  let isCollapsing = (document.querySelector(`tr[data-rp-count="${parent_count}"] .rp-toggle`).getAttribute('aria-expanded') === 'true');
  document.querySelectorAll('tr[data-rp-count]').forEach(row => {
    let count = parseInt(row.getAttribute('data-rp-count'));
    if (isCollapsing) {
      if (count === parent_count) {
        row.classList.add('rp-collapsed');
        row.querySelector('.rp-toggle').setAttribute('aria-expanded', 'false');
        row.querySelector('.rp-toggle').textContent = '+';
      }
      if (count > parent_count && count <= last_child) {
        row.classList.add('rp-hidden');
      }
    }
    else {
      // is expanding
      if (count === parent_count) {
        row.classList.remove('rp-collapsed');
        row.querySelector('.rp-toggle').setAttribute('aria-expanded', 'true');
        row.querySelector('.rp-toggle').textContent = '-';
      }
      if (count > parent_count && count <= last_child) {
        row.classList.remove('rp-collapsed');
        row.classList.remove('rp-hidden');
        row.querySelector('.rp-toggle').setAttribute('aria-expanded', 'true');
        row.querySelector('.rp-toggle').textContent = '-';
      }
    }
  });
}

document.addEventListener('keydown', function (event) {
  if (event.key === "Escape") {
    document.querySelectorAll('#rp-table details').forEach((detail) => {
      detail.removeAttribute('open');
    });
  }
});
