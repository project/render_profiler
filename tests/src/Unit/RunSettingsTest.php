<?php

namespace Drupal\Tests\render_profiler\Unit;

use Drupal\render_profiler\RunSettings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\render_profiler\RunSettings
 * @group render_profiler
 */
class RunSettingsTest extends UnitTestCase {

  /**
   * @covers ::isActive
   */
  public function testIsActive() {
    $requestStack = $this->prophesize(RequestStack::class);
    $currentUser = $this->prophesize(AccountProxyInterface::class);
    $configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $cache = $this->prophesize(CacheBackendInterface::class);

    // A request with the start parameter and proper permissions will activate
    // the profiler.
    $request = new Request();
    $request->query->add(['RENDER_PROFILER' => '1']); // add query params
    $requestStack->getCurrentRequest()->willReturn($request);
    $currentUser->hasPermission('use render_profiler')->willReturn(TRUE);
    $config = $this->prophesize(ImmutableConfig::class);
    $configFactory->get('render_profiler.settings')
      ->willReturn($config->reveal());
    $runSettings = new RunSettings(
      $requestStack->reveal(),
      $currentUser->reveal(),
      $configFactory->reveal(),
      $cache->reveal()
    );
    $this->assertTrue($runSettings->isActive());

    // A user without permission should not activate the profiler.
    $request = new Request();
    $request->query->add(['RENDER_PROFILER' => '1']); // add query params
    $requestStack->getCurrentRequest()->willReturn($request);
    $currentUser->hasPermission('use render_profiler')->willReturn(FALSE);
    $config = $this->prophesize(ImmutableConfig::class);
    $configFactory->get('render_profiler.settings')
      ->willReturn($config->reveal());
    $runSettings = new RunSettings(
      $requestStack->reveal(),
      $currentUser->reveal(),
      $configFactory->reveal(),
      $cache->reveal()
    );
    $this->assertFalse($runSettings->isActive());

    // A request without the starting parameter should not activate the profiler.
    $request = new Request();
    $requestStack->getCurrentRequest()->willReturn($request);
    $currentUser->hasPermission('use render_profiler')->willReturn(TRUE);
    $config = $this->prophesize(ImmutableConfig::class);
    $configFactory->get('render_profiler.settings')
      ->willReturn($config->reveal());
    $runSettings = new RunSettings(
      $requestStack->reveal(),
      $currentUser->reveal(),
      $configFactory->reveal(),
      $cache->reveal()
    );
    $this->assertFalse($runSettings->isActive());

    // A request with the start parameter and 'bypass_permission_check' enabled
    // should activate the profiler, even if the user does not have permission.
    $request = new Request();
    $request->query->add(['RENDER_PROFILER' => '1']); // add query params
    $requestStack->getCurrentRequest()->willReturn($request);
    $currentUser->hasPermission('use render_profiler')->willReturn(FALSE);
    $config->get('bypass_permission_check')->willReturn(TRUE);
    $configFactory->get('render_profiler.settings')
      ->willReturn($config->reveal());
    $runSettings = new RunSettings(
      $requestStack->reveal(),
      $currentUser->reveal(),
      $configFactory->reveal(),
      $cache->reveal()
    );
    $this->assertTrue($runSettings->isActive());
  }

}
