<?php

namespace Drupal\Tests\render_profiler\Unit;

use Drupal\render_profiler\Tracer;
use Drupal\Tests\UnitTestCase;

/**
 * Test the functionality of the Tracer class.
 *
 * @coversDefaultClass \Drupal\render_profiler\Tracer
 * @group render_profiler
 */
class TracerTest extends UnitTestCase {

  private Tracer $tracer;

  protected function setUp(): void {
    parent::setUp();

    $this->tracer = new Tracer();
  }

  /**
   * @covers ::getCount
   * @covers ::incrementCount
   */
  public function testCount(): void {
    $this->assertSame(0, $this->tracer->getCount());
    $this->tracer->incrementCount();
    $this->assertSame(1, $this->tracer->getCount());
  }

  /**
   * @covers ::getDepth
   * @covers ::ascend
   * @covers ::descend
   */
  public function testDepth(): void {
    $this->assertSame(0, $this->tracer->getDepth());
    $this->tracer->descend();
    $this->assertSame(1, $this->tracer->getDepth());

    $this->tracer->descend();
    $this->tracer->descend();
    $this->assertSame(3, $this->tracer->getDepth());

    $this->tracer->ascend();
    $this->assertSame(2, $this->tracer->getDepth());

    $this->tracer->ascend();
    $this->tracer->ascend();
    $this->assertSame(0, $this->tracer->getDepth());
  }

  /**
   * @covers ::getTrace
   * @covers ::ascend
   * @covers ::descend
   */
  public function testTrace(): void {
    $this->tracer->incrementCount();
    $this->tracer->descend();
    $this->assertSame([1], $this->tracer->getTrace());

    $this->tracer->incrementCount();
    $this->tracer->incrementCount();
    $this->tracer->descend();
    $this->assertSame([1, 3], $this->tracer->getTrace());

    $this->tracer->incrementCount();
    $this->tracer->descend();
    $this->assertSame([1, 3, 4], $this->tracer->getTrace());

    $this->tracer->incrementCount();
    $this->tracer->ascend();
    $this->assertSame([1, 3], $this->tracer->getTrace());

    $this->tracer->incrementCount();
    $this->tracer->descend();
    $this->assertSame([1, 3, 6], $this->tracer->getTrace());

    $this->tracer->incrementCount();
    $this->tracer->ascend();
    $this->tracer->ascend();
    $this->assertSame([1], $this->tracer->getTrace());
  }

}
